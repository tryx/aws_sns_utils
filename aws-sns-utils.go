package aws_sns_utils

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
)

func S3EventRecordsFromSNS(event events.SNSEvent) (err error, s3EventRecords []events.S3EventRecord) {

	for _, snsEventRecord := range event.Records {
		var s3Event events.S3Event
		err = json.Unmarshal([]byte(snsEventRecord.SNS.Message), &s3Event)
		if err != nil {
			return
		}

		for _, s3EventRecord := range s3Event.Records {
			s3EventRecords = append(s3EventRecords, s3EventRecord)
		}
	}
	return
}
